/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { CalendarTestModule } from '../../../test.module';
import { TableeDeleteDialogComponent } from 'app/entities/tablee/tablee-delete-dialog.component';
import { TableeService } from 'app/entities/tablee/tablee.service';

describe('Component Tests', () => {
    describe('Tablee Management Delete Component', () => {
        let comp: TableeDeleteDialogComponent;
        let fixture: ComponentFixture<TableeDeleteDialogComponent>;
        let service: TableeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CalendarTestModule],
                declarations: [TableeDeleteDialogComponent]
            })
                .overrideTemplate(TableeDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(TableeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TableeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
