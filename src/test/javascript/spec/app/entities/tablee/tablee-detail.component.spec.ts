/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CalendarTestModule } from '../../../test.module';
import { TableeDetailComponent } from 'app/entities/tablee/tablee-detail.component';
import { Tablee } from 'app/shared/model/tablee.model';

describe('Component Tests', () => {
    describe('Tablee Management Detail Component', () => {
        let comp: TableeDetailComponent;
        let fixture: ComponentFixture<TableeDetailComponent>;
        const route = ({ data: of({ tablee: new Tablee(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CalendarTestModule],
                declarations: [TableeDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(TableeDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(TableeDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.tablee).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
