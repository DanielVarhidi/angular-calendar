/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { CalendarTestModule } from '../../../test.module';
import { TableeUpdateComponent } from 'app/entities/tablee/tablee-update.component';
import { TableeService } from 'app/entities/tablee/tablee.service';
import { Tablee } from 'app/shared/model/tablee.model';

describe('Component Tests', () => {
    describe('Tablee Management Update Component', () => {
        let comp: TableeUpdateComponent;
        let fixture: ComponentFixture<TableeUpdateComponent>;
        let service: TableeService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CalendarTestModule],
                declarations: [TableeUpdateComponent]
            })
                .overrideTemplate(TableeUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(TableeUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TableeService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Tablee(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.tablee = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Tablee();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.tablee = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
