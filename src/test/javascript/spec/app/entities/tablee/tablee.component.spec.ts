/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CalendarTestModule } from '../../../test.module';
import { TableeComponent } from 'app/entities/tablee/tablee.component';
import { TableeService } from 'app/entities/tablee/tablee.service';
import { Tablee } from 'app/shared/model/tablee.model';

describe('Component Tests', () => {
    describe('Tablee Management Component', () => {
        let comp: TableeComponent;
        let fixture: ComponentFixture<TableeComponent>;
        let service: TableeService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [CalendarTestModule],
                declarations: [TableeComponent],
                providers: []
            })
                .overrideTemplate(TableeComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(TableeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TableeService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Tablee(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.tablees[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
