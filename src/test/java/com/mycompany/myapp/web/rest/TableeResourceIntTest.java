package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.CalendarApp;

import com.mycompany.myapp.domain.Tablee;
import com.mycompany.myapp.repository.TableeRepository;
import com.mycompany.myapp.service.TableeService;
import com.mycompany.myapp.service.dto.TableeDTO;
import com.mycompany.myapp.service.mapper.TableeMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TableeResource REST controller.
 *
 * @see TableeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CalendarApp.class)
public class TableeResourceIntTest {

    private static final Integer DEFAULT_NUMBER_OF_SEAT = 1;
    private static final Integer UPDATED_NUMBER_OF_SEAT = 2;

    @Autowired
    private TableeRepository tableeRepository;

    @Autowired
    private TableeMapper tableeMapper;
    
    @Autowired
    private TableeService tableeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTableeMockMvc;

    private Tablee tablee;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TableeResource tableeResource = new TableeResource(tableeService);
        this.restTableeMockMvc = MockMvcBuilders.standaloneSetup(tableeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tablee createEntity(EntityManager em) {
        Tablee tablee = new Tablee()
            .numberOfSeat(DEFAULT_NUMBER_OF_SEAT);
        return tablee;
    }

    @Before
    public void initTest() {
        tablee = createEntity(em);
    }

    @Test
    @Transactional
    public void createTablee() throws Exception {
        int databaseSizeBeforeCreate = tableeRepository.findAll().size();

        // Create the Tablee
        TableeDTO tableeDTO = tableeMapper.toDto(tablee);
        restTableeMockMvc.perform(post("/api/tablees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tableeDTO)))
            .andExpect(status().isCreated());

        // Validate the Tablee in the database
        List<Tablee> tableeList = tableeRepository.findAll();
        assertThat(tableeList).hasSize(databaseSizeBeforeCreate + 1);
        Tablee testTablee = tableeList.get(tableeList.size() - 1);
        assertThat(testTablee.getNumberOfSeat()).isEqualTo(DEFAULT_NUMBER_OF_SEAT);
    }

    @Test
    @Transactional
    public void createTableeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tableeRepository.findAll().size();

        // Create the Tablee with an existing ID
        tablee.setId(1L);
        TableeDTO tableeDTO = tableeMapper.toDto(tablee);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTableeMockMvc.perform(post("/api/tablees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tableeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tablee in the database
        List<Tablee> tableeList = tableeRepository.findAll();
        assertThat(tableeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTablees() throws Exception {
        // Initialize the database
        tableeRepository.saveAndFlush(tablee);

        // Get all the tableeList
        restTableeMockMvc.perform(get("/api/tablees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tablee.getId().intValue())))
            .andExpect(jsonPath("$.[*].numberOfSeat").value(hasItem(DEFAULT_NUMBER_OF_SEAT)));
    }
    
    @Test
    @Transactional
    public void getTablee() throws Exception {
        // Initialize the database
        tableeRepository.saveAndFlush(tablee);

        // Get the tablee
        restTableeMockMvc.perform(get("/api/tablees/{id}", tablee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tablee.getId().intValue()))
            .andExpect(jsonPath("$.numberOfSeat").value(DEFAULT_NUMBER_OF_SEAT));
    }

    @Test
    @Transactional
    public void getNonExistingTablee() throws Exception {
        // Get the tablee
        restTableeMockMvc.perform(get("/api/tablees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTablee() throws Exception {
        // Initialize the database
        tableeRepository.saveAndFlush(tablee);

        int databaseSizeBeforeUpdate = tableeRepository.findAll().size();

        // Update the tablee
        Tablee updatedTablee = tableeRepository.findById(tablee.getId()).get();
        // Disconnect from session so that the updates on updatedTablee are not directly saved in db
        em.detach(updatedTablee);
        updatedTablee
            .numberOfSeat(UPDATED_NUMBER_OF_SEAT);
        TableeDTO tableeDTO = tableeMapper.toDto(updatedTablee);

        restTableeMockMvc.perform(put("/api/tablees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tableeDTO)))
            .andExpect(status().isOk());

        // Validate the Tablee in the database
        List<Tablee> tableeList = tableeRepository.findAll();
        assertThat(tableeList).hasSize(databaseSizeBeforeUpdate);
        Tablee testTablee = tableeList.get(tableeList.size() - 1);
        assertThat(testTablee.getNumberOfSeat()).isEqualTo(UPDATED_NUMBER_OF_SEAT);
    }

    @Test
    @Transactional
    public void updateNonExistingTablee() throws Exception {
        int databaseSizeBeforeUpdate = tableeRepository.findAll().size();

        // Create the Tablee
        TableeDTO tableeDTO = tableeMapper.toDto(tablee);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTableeMockMvc.perform(put("/api/tablees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tableeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tablee in the database
        List<Tablee> tableeList = tableeRepository.findAll();
        assertThat(tableeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTablee() throws Exception {
        // Initialize the database
        tableeRepository.saveAndFlush(tablee);

        int databaseSizeBeforeDelete = tableeRepository.findAll().size();

        // Get the tablee
        restTableeMockMvc.perform(delete("/api/tablees/{id}", tablee.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Tablee> tableeList = tableeRepository.findAll();
        assertThat(tableeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tablee.class);
        Tablee tablee1 = new Tablee();
        tablee1.setId(1L);
        Tablee tablee2 = new Tablee();
        tablee2.setId(tablee1.getId());
        assertThat(tablee1).isEqualTo(tablee2);
        tablee2.setId(2L);
        assertThat(tablee1).isNotEqualTo(tablee2);
        tablee1.setId(null);
        assertThat(tablee1).isNotEqualTo(tablee2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TableeDTO.class);
        TableeDTO tableeDTO1 = new TableeDTO();
        tableeDTO1.setId(1L);
        TableeDTO tableeDTO2 = new TableeDTO();
        assertThat(tableeDTO1).isNotEqualTo(tableeDTO2);
        tableeDTO2.setId(tableeDTO1.getId());
        assertThat(tableeDTO1).isEqualTo(tableeDTO2);
        tableeDTO2.setId(2L);
        assertThat(tableeDTO1).isNotEqualTo(tableeDTO2);
        tableeDTO1.setId(null);
        assertThat(tableeDTO1).isNotEqualTo(tableeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tableeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tableeMapper.fromId(null)).isNull();
    }
}
