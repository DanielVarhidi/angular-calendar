package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tablee.
 */
@Entity
@Table(name = "tablee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Tablee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number_of_seat")
    private Integer numberOfSeat;

    @OneToMany(mappedBy = "tablee")
   // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Reservation> reservations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberOfSeat() {
        return numberOfSeat;
    }

    public Tablee numberOfSeat(Integer numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
        return this;
    }

    public void setNumberOfSeat(Integer numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public Set<Reservation> getReservations() {
        return reservations;
    }

    public Tablee reservations(Set<Reservation> reservations) {
        this.reservations = reservations;
        return this;
    }

    public Tablee addReservation(Reservation reservation) {
        this.reservations.add(reservation);
        reservation.setTablee(this);
        return this;
    }

    public Tablee removeReservation(Reservation reservation) {
        this.reservations.remove(reservation);
        reservation.setTablee(null);
        return this;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tablee tablee = (Tablee) o;
        if (tablee.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tablee.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tablee{" +
            "id=" + getId() +
            ", numberOfSeat=" + getNumberOfSeat() +
            "}";
    }
}
