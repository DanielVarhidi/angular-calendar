package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Reservation.
 */
@Entity
@Table(name = "reservation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "reservation_start")
    private ZonedDateTime reservationStart;

    @Column(name = "reservation_end")
    private ZonedDateTime reservationEnd;

    @Column(name = "name")
    private String name;

    @Column(name = "note")
    private String note;

    @ManyToOne
    @JsonIgnoreProperties("reservations")
    private Tablee tablee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getReservationStart() {
        return reservationStart;
    }

    public Reservation reservationStart(ZonedDateTime reservationStart) {
        this.reservationStart = reservationStart;
        return this;
    }

    public void setReservationStart(ZonedDateTime reservationStart) {
        this.reservationStart = reservationStart;
    }

    public ZonedDateTime getReservationEnd() {
        return reservationEnd;
    }

    public Reservation reservationEnd(ZonedDateTime reservationEnd) {
        this.reservationEnd = reservationEnd;
        return this;
    }

    public void setReservationEnd(ZonedDateTime reservationEnd) {
        this.reservationEnd = reservationEnd;
    }

    public String getName() {
        return name;
    }

    public Reservation name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public Reservation note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Tablee getTablee() {
        return tablee;
    }

    public Reservation tablee(Tablee tablee) {
        this.tablee = tablee;
        return this;
    }

    public void setTablee(Tablee tablee) {
        this.tablee = tablee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reservation reservation = (Reservation) o;
        if (reservation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Reservation{" +
            "id=" + getId() +
            ", reservationStart='" + getReservationStart() + "'" +
            ", reservationEnd='" + getReservationEnd() + "'" +
            ", name='" + getName() + "'" +
            ", note='" + getNote() + "'" +
            "}";
    }
}
