package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.TableeService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.service.dto.TableeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tablee.
 */
@RestController
@RequestMapping("/api")
public class TableeResource {

    private final Logger log = LoggerFactory.getLogger(TableeResource.class);

    private static final String ENTITY_NAME = "tablee";

    private final TableeService tableeService;

    public TableeResource(TableeService tableeService) {
        this.tableeService = tableeService;
    }

    /**
     * POST  /tablees : Create a new tablee.
     *
     * @param tableeDTO the tableeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tableeDTO, or with status 400 (Bad Request) if the tablee has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tablees")
    @Timed
    public ResponseEntity<TableeDTO> createTablee(@RequestBody TableeDTO tableeDTO) throws URISyntaxException {
        log.debug("REST request to save Tablee : {}", tableeDTO);
        if (tableeDTO.getId() != null) {
            throw new BadRequestAlertException("A new tablee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TableeDTO result = tableeService.save(tableeDTO);
        return ResponseEntity.created(new URI("/api/tablees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tablees : Updates an existing tablee.
     *
     * @param tableeDTO the tableeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tableeDTO,
     * or with status 400 (Bad Request) if the tableeDTO is not valid,
     * or with status 500 (Internal Server Error) if the tableeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tablees")
    @Timed
    public ResponseEntity<TableeDTO> updateTablee(@RequestBody TableeDTO tableeDTO) throws URISyntaxException {
        log.debug("REST request to update Tablee : {}", tableeDTO);
        if (tableeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TableeDTO result = tableeService.save(tableeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tableeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tablees : get all the tablees.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tablees in body
     */
    @GetMapping("/tablees")
    @Timed
    public List<TableeDTO> getAllTablees() {
        log.debug("REST request to get all Tablees");
        return tableeService.findAll();
    }

    /**
     * GET  /tablees/:id : get the "id" tablee.
     *
     * @param id the id of the tableeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tableeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/tablees/{id}")
    @Timed
    public ResponseEntity<TableeDTO> getTablee(@PathVariable Long id) {
        log.debug("REST request to get Tablee : {}", id);
        Optional<TableeDTO> tableeDTO = tableeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tableeDTO);
    }

    /**
     * DELETE  /tablees/:id : delete the "id" tablee.
     *
     * @param id the id of the tableeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tablees/{id}")
    @Timed
    public ResponseEntity<Void> deleteTablee(@PathVariable Long id) {
        log.debug("REST request to delete Tablee : {}", id);
        tableeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
