package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Tablee;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Tablee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TableeRepository extends JpaRepository<Tablee, Long> {

}
