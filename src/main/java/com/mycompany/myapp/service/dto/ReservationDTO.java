package com.mycompany.myapp.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Reservation entity.
 */
public class ReservationDTO implements Serializable {

    private Long id;

    private ZonedDateTime reservationStart;

    private ZonedDateTime reservationEnd;

    private String name;

    private String note;

    private Long tableeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getReservationStart() {
        return reservationStart;
    }

    public void setReservationStart(ZonedDateTime reservationStart) {
        this.reservationStart = reservationStart;
    }

    public ZonedDateTime getReservationEnd() {
        return reservationEnd;
    }

    public void setReservationEnd(ZonedDateTime reservationEnd) {
        this.reservationEnd = reservationEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getTableeId() {
        return tableeId;
    }

    public void setTableeId(Long tableeId) {
        this.tableeId = tableeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReservationDTO reservationDTO = (ReservationDTO) o;
        if (reservationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReservationDTO{" +
            "id=" + getId() +
            ", reservationStart='" + getReservationStart() + "'" +
            ", reservationEnd='" + getReservationEnd() + "'" +
            ", name='" + getName() + "'" +
            ", note='" + getNote() + "'" +
            ", tablee=" + getTableeId() +
            "}";
    }
}
