package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the Tablee entity.
 */
public class TableeDTO implements Serializable {

    private Long id;

    private Integer numberOfSeat;

    private Set<ReservationDTO> reservations = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(Integer numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    public Set<ReservationDTO> getReservations() {
        return reservations;
    }

    public void setReservations(Set<ReservationDTO> reservations) {
        this.reservations = reservations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TableeDTO tableeDTO = (TableeDTO) o;
        if (tableeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tableeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TableeDTO{" +
            "id=" + getId() +
            ", numberOfSeat=" + getNumberOfSeat() +
            "}";
    }
}
