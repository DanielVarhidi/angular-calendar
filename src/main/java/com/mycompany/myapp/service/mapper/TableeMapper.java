package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.TableeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Tablee and its DTO TableeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TableeMapper extends EntityMapper<TableeDTO, Tablee> {

    default Tablee fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tablee tablee = new Tablee();
        tablee.setId(id);
        return tablee;
    }
}
