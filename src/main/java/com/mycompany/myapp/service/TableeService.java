package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.TableeDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Tablee.
 */
public interface TableeService {

    /**
     * Save a tablee.
     *
     * @param tableeDTO the entity to save
     * @return the persisted entity
     */
    TableeDTO save(TableeDTO tableeDTO);

    /**
     * Get all the tablees.
     *
     * @return the list of entities
     */
    List<TableeDTO> findAll();


    /**
     * Get the "id" tablee.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<TableeDTO> findOne(Long id);

    /**
     * Delete the "id" tablee.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
