package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.TableeService;
import com.mycompany.myapp.domain.Tablee;
import com.mycompany.myapp.repository.TableeRepository;
import com.mycompany.myapp.service.dto.TableeDTO;
import com.mycompany.myapp.service.mapper.TableeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * Service Implementation for managing Tablee.
 */
@Service
@Transactional
public class TableeServiceImpl implements TableeService {

    private final Logger log = LoggerFactory.getLogger(TableeServiceImpl.class);

    private final TableeRepository tableeRepository;

    private final TableeMapper tableeMapper;

    public TableeServiceImpl(TableeRepository tableeRepository, TableeMapper tableeMapper) {
        this.tableeRepository = tableeRepository;
        this.tableeMapper = tableeMapper;
    }

    /**
     * Save a tablee.
     *
     * @param tableeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TableeDTO save(TableeDTO tableeDTO) {
        log.debug("Request to save Tablee : {}", tableeDTO);
        Tablee tablee = tableeMapper.toEntity(tableeDTO);
        tablee = tableeRepository.save(tablee);
        return tableeMapper.toDto(tablee);
    }

    /**
     * Get all the tablees.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TableeDTO> findAll() {
        log.debug("Request to get all Tablees");
        return tableeRepository.findAll().stream()
            .map(tableeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one tablee by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TableeDTO> findOne(Long id) {
        log.debug("Request to get Tablee : {}", id);
        return tableeRepository.findById(id)
            .map(tableeMapper::toDto);
    }

    /**
     * Delete the tablee by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Tablee : {}", id);
        tableeRepository.deleteById(id);
    }
}
