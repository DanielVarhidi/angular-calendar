import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CalendarSharedModule } from 'app/shared';
import { tableePopupRoute } from '../tablee';
import { calendarRoute } from './calendar.route';
import { CalendarComponent } from './calendar.component';
import { ScheduleModule } from 'primeng/schedule';

const ENTITY_STATES = [...calendarRoute, ...tableePopupRoute];

@NgModule({
    imports: [CalendarSharedModule, RouterModule.forChild(ENTITY_STATES), ScheduleModule],
    declarations: [CalendarComponent],
    entryComponents: [CalendarComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CalendarCalendarModule {}
