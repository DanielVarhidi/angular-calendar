import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITablee } from 'app/shared/model/tablee.model';
import { Principal } from 'app/core';
import { TableeService } from '../tablee';

@Component({
    selector: 'jhi-calendar',
    templateUrl: './calendar.component.html'
})
export class CalendarComponent implements OnInit, OnDestroy {
    tablees: ITablee[];
    currentAccount: any;
    eventSubscriber: Subscription;
    events: any[];

    constructor(
        private tableeService: TableeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.tableeService.query().subscribe(
            (res: HttpResponse<ITablee[]>) => {
                this.tablees = res.body;
                this.setData();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInTablees();
    }

    setData() {
        this.events = [
            {
                title: 'All Day Event',
                start: '2016-01-01'
            },
            {
                title: 'Long Event',
                start: '2016-01-07',
                end: '2016-01-10'
            },
            {
                title: 'Repeating Event',
                start: '2016-01-09T16:00:00'
            },
            {
                title: 'Repeating Event',
                start: '2016-01-16T16:00:00'
            },
            {
                title: 'Conference',
                start: '2016-01-11',
                end: '2016-01-13'
            }
        ];
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ITablee) {
        return item.id;
    }

    registerChangeInTablees() {
        this.eventSubscriber = this.eventManager.subscribe('tableeListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
