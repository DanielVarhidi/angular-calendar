import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tablee } from 'app/shared/model/tablee.model';
import { ITablee } from 'app/shared/model/tablee.model';
import { CalendarComponent } from './calendar.component';
import { TableeService } from '../tablee';

export const calendarRoute: Routes = [
    {
        path: 'calendar',
        component: CalendarComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Tablees'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const calendarPopupRoute: Routes = [];
