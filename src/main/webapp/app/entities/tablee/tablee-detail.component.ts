import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITablee } from 'app/shared/model/tablee.model';

@Component({
    selector: 'jhi-tablee-detail',
    templateUrl: './tablee-detail.component.html'
})
export class TableeDetailComponent implements OnInit {
    tablee: ITablee;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ tablee }) => {
            this.tablee = tablee;
        });
    }

    previousState() {
        window.history.back();
    }
}
