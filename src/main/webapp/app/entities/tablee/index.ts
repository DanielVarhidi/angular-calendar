export * from './tablee.service';
export * from './tablee-update.component';
export * from './tablee-delete-dialog.component';
export * from './tablee-detail.component';
export * from './tablee.component';
export * from './tablee.route';
