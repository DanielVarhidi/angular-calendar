import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CalendarSharedModule } from 'app/shared';
import {
    TableeComponent,
    TableeDetailComponent,
    TableeUpdateComponent,
    TableeDeletePopupComponent,
    TableeDeleteDialogComponent,
    tableeRoute,
    tableePopupRoute
} from './';

const ENTITY_STATES = [...tableeRoute, ...tableePopupRoute];

@NgModule({
    imports: [CalendarSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [TableeComponent, TableeDetailComponent, TableeUpdateComponent, TableeDeleteDialogComponent, TableeDeletePopupComponent],
    entryComponents: [TableeComponent, TableeUpdateComponent, TableeDeleteDialogComponent, TableeDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CalendarTableeModule {}
