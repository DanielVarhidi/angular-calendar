import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITablee } from 'app/shared/model/tablee.model';

type EntityResponseType = HttpResponse<ITablee>;
type EntityArrayResponseType = HttpResponse<ITablee[]>;

@Injectable({ providedIn: 'root' })
export class TableeService {
    private resourceUrl = SERVER_API_URL + 'api/tablees';

    constructor(private http: HttpClient) {}

    create(tablee: ITablee): Observable<EntityResponseType> {
        return this.http.post<ITablee>(this.resourceUrl, tablee, { observe: 'response' });
    }

    update(tablee: ITablee): Observable<EntityResponseType> {
        return this.http.put<ITablee>(this.resourceUrl, tablee, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ITablee>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ITablee[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
