import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tablee } from 'app/shared/model/tablee.model';
import { TableeService } from './tablee.service';
import { TableeComponent } from './tablee.component';
import { TableeDetailComponent } from './tablee-detail.component';
import { TableeUpdateComponent } from './tablee-update.component';
import { TableeDeletePopupComponent } from './tablee-delete-dialog.component';
import { ITablee } from 'app/shared/model/tablee.model';
import { CalendarComponent } from '../FullCalendar/calendar.component';

@Injectable({ providedIn: 'root' })
export class TableeResolve implements Resolve<ITablee> {
    constructor(private service: TableeService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((tablee: HttpResponse<Tablee>) => tablee.body));
        }
        return of(new Tablee());
    }
}

export const tableeRoute: Routes = [
    {
        path: 'tablee',
        component: TableeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Tablees'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tablee/calendar-view',
        component: CalendarComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Calendar'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tablee/:id/view',
        component: TableeDetailComponent,
        resolve: {
            tablee: TableeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Tablees'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tablee/new',
        component: TableeUpdateComponent,
        resolve: {
            tablee: TableeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Tablees'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'tablee/:id/edit',
        component: TableeUpdateComponent,
        resolve: {
            tablee: TableeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Tablees'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tableePopupRoute: Routes = [
    {
        path: 'tablee/:id/delete',
        component: TableeDeletePopupComponent,
        resolve: {
            tablee: TableeResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Tablees'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
