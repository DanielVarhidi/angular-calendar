import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ITablee } from 'app/shared/model/tablee.model';
import { TableeService } from './tablee.service';

@Component({
    selector: 'jhi-tablee-update',
    templateUrl: './tablee-update.component.html'
})
export class TableeUpdateComponent implements OnInit {
    private _tablee: ITablee;
    isSaving: boolean;

    constructor(private tableeService: TableeService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ tablee }) => {
            this.tablee = tablee;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.tablee.id !== undefined) {
            this.subscribeToSaveResponse(this.tableeService.update(this.tablee));
        } else {
            this.subscribeToSaveResponse(this.tableeService.create(this.tablee));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ITablee>>) {
        result.subscribe((res: HttpResponse<ITablee>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get tablee() {
        return this._tablee;
    }

    set tablee(tablee: ITablee) {
        this._tablee = tablee;
    }
}
