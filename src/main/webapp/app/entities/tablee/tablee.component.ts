import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITablee } from 'app/shared/model/tablee.model';
import { Principal } from 'app/core';
import { TableeService } from './tablee.service';

@Component({
    selector: 'jhi-tablee',
    templateUrl: './tablee.component.html'
})
export class TableeComponent implements OnInit, OnDestroy {
    tablees: ITablee[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private tableeService: TableeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.tableeService.query().subscribe(
            (res: HttpResponse<ITablee[]>) => {
                this.tablees = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInTablees();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ITablee) {
        return item.id;
    }

    registerChangeInTablees() {
        this.eventSubscriber = this.eventManager.subscribe('tableeListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
