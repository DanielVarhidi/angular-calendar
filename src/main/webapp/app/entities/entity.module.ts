import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CalendarTableeModule } from './tablee/tablee.module';
import { CalendarReservationModule } from './reservation/reservation.module';
import { CalendarCalendarModule } from './FullCalendar/calendar.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        CalendarTableeModule,
        CalendarReservationModule,
        CalendarCalendarModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CalendarEntityModule {}
