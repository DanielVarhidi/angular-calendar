import { NgModule } from '@angular/core';

import { CalendarSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [CalendarSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [CalendarSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class CalendarSharedCommonModule {}
