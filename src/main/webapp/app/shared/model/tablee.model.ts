import { IReservation } from 'app/shared/model//reservation.model';

export interface ITablee {
    id?: number;
    numberOfSeat?: number;
    reservations?: IReservation[];
}

export class Tablee implements ITablee {
    constructor(public id?: number, public numberOfSeat?: number, public reservations?: IReservation[]) {}
}
