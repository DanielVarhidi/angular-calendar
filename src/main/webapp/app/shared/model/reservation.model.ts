import { Moment } from 'moment';

export interface IReservation {
    id?: number;
    reservationStart?: Moment;
    reservationEnd?: Moment;
    name?: string;
    note?: string;
    tableeId?: number;
}

export class Reservation implements IReservation {
    constructor(
        public id?: number,
        public reservationStart?: Moment,
        public reservationEnd?: Moment,
        public name?: string,
        public note?: string,
        public tableeId?: number
    ) {}
}
